#!/bin/bash
set -eu -o pipefail

module purge

. ./config

version="$1"
intel_module="$base_modules_dir/intel/$version"
intel_modulepath="$modules_root/intel-$version"
intelmpi_module="$intel_modulepath/intelmpi/$version"
intelmpi_modulepath="$modules_root/intel-$version-intelmpi-$version"

mkdir "$intel_modulepath"
mkdir "$intelmpi_modulepath"
chmod g+w "$intel_modulepath"
chmod g+w "$intelmpi_modulepath"

mkdir -p "$(dirname "$intel_module")"
mkdir -p "$(dirname "$intelmpi_module")"
chmod g+w "$(dirname "$intel_module")"
chmod g+w "$(dirname "$intelmpi_module")"

cat >$intel_module <<___
#%Module -*- tcl -*-

family compiler

prepend-path MODULEPATH $intel_modulepath

setenv INTEL_LICENSE_FILE $intel_license_file

setenv CC  icc
setenv CXX icpc
setenv F77 ifort
setenv F90 ifort
setenv FC  ifort

setenv __INTEL_PRE_CFLAGS "-gcc-name=core-gcc -gxx-name=core-g++"
setenv __INTEL_PRE_FFLAGS "-gcc-name=core-gcc -gxx-name=core-g++"
___

cat  >"$intelmpi_module" <intelmpi.template
cat >>"$intelmpi_module" <<___

prepend-path MODULEPATH $intelmpi_modulepath

setenv I_MPI_CC  icc
setenv I_MPI_CXX icpc
setenv I_MPI_F77 ifort
setenv I_MPI_F90 ifort
setenv I_MPI_FC  ifort

___

# if MANPATH isn't set compilervars.sh will insert the output of `manpath`,
# so make sure that doesn't happen
fullmodule=$(MANPATH=/usr/share/man ../utils/diffenv source $intel_root/$version/bin/compilervars.sh intel64 \
    | grep -v 'INTEL_LICENSE_FILE' \
    | sed 's:/gpfs/fs1::g' \
    | ../utils/diffenv2module)

cat <<<"$fullmodule" \
    | egrep -v '_MPI|/mpi/' \
    | cat -s \
    >> $intel_module

cat <<<"$fullmodule" \
    | grep 'set prefix' \
    >> $intelmpi_module

cat <<<"$fullmodule" \
    | egrep '_MPI|/mpi/' \
    | egrep -v '#' \
    >> $intelmpi_module

chmod a=r "$intel_module" "$intelmpi_module"

echo ok
