#!/bin/bash
set -eu -o pipefail

## utilities

usage() {
    echo "Usage: cleanenv [BASH SCRIPT [ARG]...]"
}

error() {
    echo "cleanenv:" "$@" >> /dev/stderr
    exit 1
}

assert_not_exists() {
    [[ ! -e $1 ]] || error "already exists: $1"
}

assert_is_file() {
    [[ -f $1 ]] || error "file not found: $1"
}

assert_is_dir() {
    [[ -d $1 ]] || error "directory not found: $1"
}

## defaults

bash_env=(
    "HOSTNAME=$HOSTNAME"
    "LC_ALL=C"
    "LOGNAME=$LOGNAME"
    "MANPATH=/usr/share/man"
    "PATH=$(realpath "${BASH_SOURCE[0]}")-bin:/usr/bin:/bin"
    "TERM=$TERM"
    "USER=$USER"
)
bash_rcfile=${CLEANENV_RCFILE-}
bash_long_opts=()
bash_short_opts=()
bash_args=()
logfile="cleanenv.log"
workdir=""

## options

while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            exit 0
            ;;

        --env)
            bash_env+=("$2")
            shift
            shift
            ;;

        --home)
            workdir="$2"
            [[ -d $workdir ]] || error "home directory doesn't exist: $workdir"
            shift
            shift
            ;;

        --logfile)
            logfile="$2"
            shift
            shift
            ;;

        --rcfile)
            bash_rcfile="$2"
            shift
            shift
            ;;

        --) shift; break ;;
        *) break ;;
    esac
done

## interactive?

if [[ $# -gt 0 ]]; then
    # non-interactive
    assert_is_file "$1"
    bash_short_opts+=(-eu -o pipefail)
    bash_args=("$@")
    bash_args[0]=$(realpath $1)
else
    # interactive
    bash_env+=("PS1=cleanenv> ")
    bash_short_opts+=(-i)
fi

## environment file

if [[ -n $bash_rcfile ]]; then
    assert_is_file "$bash_rcfile"
    bash_rcfile=$(realpath $bash_rcfile)
    bash_env+=("BASH_ENV=$bash_rcfile")
    bash_long_opts+=("--rcfile" "$bash_rcfile")
    bash_short_opts+=(-x)
fi

## working directory

cleanup="false"
if [[ -z $workdir ]]; then
    workdir="$(mktemp -d)"
    cleanup="true"
fi

## go

cd "$workdir"
bash_env+=("HOME=$PWD" "PWD=$PWD")

# Bash is very particular in that long opts must come before short opts.
# Also, "${x[@]}" will raise an unset error if x is an empty array.
bash_cmd=(bash)
[[ ${#bash_long_opts[@]}  -gt 0 ]] && bash_cmd+=("${bash_long_opts[@]}")
[[ ${#bash_short_opts[@]} -gt 0 ]] && bash_cmd+=("${bash_short_opts[@]}")
[[ ${#bash_args[@]}       -gt 0 ]] && bash_cmd+=(-- "${bash_args[@]}")

# can't just pipe to tee, since that masks the return status
exec &> >(tee -a $logfile)

if ! env - "${bash_env[@]}" "${bash_cmd[@]}"; then
    hr="================================================================"
    echo "$hr"
    echo "cleanenv failed!"
    echo "working directory: $workdir"
    echo "transcript: $logfile"
    echo "command: env -" "${bash_env[@]}" "${bash_cmd[@]}"
    echo "$hr"
    exit 1
fi

if [[ $cleanup == "true" ]]; then
    chmod -R +w "$workdir"
    rm -r "$workdir"
fi

exit 0
