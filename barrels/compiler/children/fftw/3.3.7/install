#!/bin/bash

prefix="$1"
module load $2

version="3.3.7"
md5sum="0d5915d7d39b3253c1cc05030d79ac47"
archive="fftw-$version.tar.gz"
url="http://www.fftw.org/$archive"

wget $url
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd fftw-$version

# fix avx512 flag bug when using the intel compilers
sed -i 's/-mavx512f/-march=skylake-avx512/' configure

for precision in float double long-double; do

    mkdir build-$precision
    cd build-$precision

    opts=""
    case $precision in
        float|double)
            opts+=" --enable-avx2"
            opts+=" --enable-avx512"
            ;;
    esac

    ../configure --prefix="$prefix" $opts \
        --enable-$precision \
        --enable-openmp \
        --enable-shared \
        --enable-threads \
        --with-pic

    make -j$(nproc)
    make check
    make install

    cd ..
done
