#!/bin/bash

prefix="$1"
module load $2
module load $3
module load fftw/3.3.8

name="lammps"
version="29Mar2019"
md5sum="f1309003494412f795330e166bd76237"
archive="patch_$version.tar.gz"
url="https://github.com/lammps/lammps/archive/$archive"

wget $url
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd $name-patch_$version

ROOT=$PWD

cd lib/poems

make -j$(nproc) -f Makefile.mpi

cd $ROOT/src

make yes-poems
make yes-manybody
make yes-user-reaxc
make yes-kspace
make yes-coreshell

# requested by the user in the Ticket#036690
make yes-molecule
make yes-class2
make yes-colloid
make yes-mc
make yes-qeq
make yes-replica
make yes-rigid
make yes-user-diffraction

# Replacing hardcoded g++
sed -i "s/g++/$CC/g" $ROOT/lib/voronoi/Install.py
make lib-voronoi args="-b -v voro++-0.4.6"
make yes-voronoi

make yes-user-misc
make yes-user-meamc

# New packages for the user kejiang
make yes-user-diffraction
make yes-misc
make yes-granular
make yes-user-cgdna
make yes-asphere

make yes-user-intel
make -j$(nproc) intel_cpu_intelmpi

mkdir $prefix/bin

cd $ROOT

cp src/lmp_intel_cpu_intelmpi $prefix/bin
cp -Rv * $prefix
ln -s $prefix/bin/lmp_intel_cpu_intelmpi $prefix/bin/lammps
ln -s $prefix/bin/lmp_intel_cpu_intelmpi $prefix/bin/lmp_mpi

# https://linuxcluster.wordpress.com/2016/06/14/compiling-lammps-14may16-with-intel-15-0-6-and-intel-mpi-5-0-3/
# http://lammps.sandia.gov/tutorials/italy14/Compiling_LAMMPS.pdf
