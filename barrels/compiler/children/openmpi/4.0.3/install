#!/bin/bash

prefix="$1"
module load "${@:2}"

cpu_arch=$(uname -m)
if [[ "$cpu_arch" == "x86_64" ]]
then
    hpcx="/scinet/niagara/mellanox/hpcx-2.5-mofed-4.6-1"
elif [[ "$cpu_arch" == "ppc64le" ]]
then
    hpcx="/scinet/mist/mellanox/hpcx-2.5-mofed-4.5-1"
else
    echo "Unknown architecture!"
    exit 1
fi

[[ -d $hpcx ]]

version="4.0.3"
archive="openmpi-${version}.tar.gz"
md5sum="f4be54a4358a536ec2cdc694c7200f0b"
url="https://www.open-mpi.org/software/ompi/v${version:0:3}/downloads/$archive"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd openmpi-$version

hcoll="$hpcx/hcoll"
sharp="$hpcx/sharp"
ucx="$hpcx/ucx"
slurm="/opt/slurm"

LD_LIBRARY_PATH="$hcoll/lib:$sharp/lib:$LD_LIBRARY_PATH"

# don't expose internal headers
# https://github.com/open-mpi/hwloc/issues/229
sed -i -e '/with_devel_headers=yes/d' contrib/platform/mellanox/optimized

./configure --prefix="$prefix" \
    --enable-mpi-cxx \
    --enable-mpi1-compatibility \
    --with-hcoll="$hcoll" \
    --with-knem="$(pkg-config --variable=prefix knem)" \
    --with-libevent=internal \
    --with-platform=contrib/platform/mellanox/optimized \
    --with-pmi="$slurm" \
    --with-slurm="$slurm" \
    --with-ucx="$ucx"

make all -j$(nproc)
make check
make install
