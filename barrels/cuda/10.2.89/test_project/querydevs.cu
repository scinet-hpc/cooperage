#include <stdio.h>
#include <cuda.h>

#define CHK_CUDA(e) {if (e != cudaSuccess) { \
                      fprintf(stderr,"Error: %s\n", cudaGetErrorString(e)); \
                      exit(-1);}\
                    }

int main(int argc, char **argv) {
	int i, count;
    cudaDeviceProp prop;

	CHK_CUDA( cudaGetDeviceCount( &count ));
    for (i=0; i<count; i++) {
        CHK_CUDA( cudaGetDeviceProperties( &prop, i ));
        printf("Device %d has:\n",i);
        printf("\tName               %s,\n",prop.name);
        printf("\tNumber of SMs      %d,\n",prop.multiProcessorCount);
        printf("\tWarp Size          %d,\n",prop.warpSize);
        printf("\tMax Threads/block  %d,\n",prop.maxThreadsPerBlock);
        printf("\tRegisgers/block    %d,\n",prop.regsPerBlock);
        printf("\tCompute Capability %d.%d,\n",prop.major, prop.minor);
        printf("\tGlobal Mem         %d MB,\n",prop.totalGlobalMem/(1024*1024));
        printf("\tMax Threads/dim   (%d,%d,%d),\n",prop.maxThreadsDim[0],
                                    prop.maxThreadsDim[1], prop.maxThreadsDim[2]);
        printf("\tMax Blocks/dim    (%d,%d,%d).\n",prop.maxGridSize[0],
                                    prop.maxGridSize[1], prop.maxGridSize[2]);
        printf("\tShared Mem/block   %d kB,\n",prop.sharedMemPerBlock/(1024));
    }
	return 0;
}
