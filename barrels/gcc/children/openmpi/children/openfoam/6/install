#!/bin/bash

###########################################################
## This script compiles OpenFOAM/6 on Niagara cluster.   ## 
## ./cooper-install gcc/7.3.0@openmpi/3.0.1@openfoam/6   ##
## F.Ertinaz | Oct, 26 2018                              ## 
###########################################################

prefix="$1"
module load $2
module load $3

module load cmake
module load boost/1.66.0 gmp/6.1.2 mpfr/4.0.1 metis/5.1.0

## This is the path assigned by cooperage: 
## /dev/shm/fertinaz/cleanenv.abcxyz
export tmpdir=$PWD

## download openfoam
pkg="OpenFOAM-6"
wget -O - http://dl.openfoam.org/source/6 | tar xz
mv OpenFOAM-6-version-6 OpenFOAM-6

## Change dir to OpenFOAM-6
cd $tmpdir/$pkg

################################################################
## CHANGE OPENFOAM CONFIGURATION FILES FOR SYSTEMOPENMPI ETC. ##
################################################################

## Install OF in the temporary directory 
sed -i 's,FOAM_INST_DIR=$HOME\/$WM_PROJECT,FOAM_INST_DIR='"$tmpdir"',g' $tmpdir/$pkg/etc/bashrc 
sed -i '45s/\[/#\[/' $tmpdir/$pkg/etc/bashrc
sed -i '46s/export/#export/' $tmpdir/$pkg/etc/bashrc

## Change MPI version
# sed -i 's,export WM_MPLIB=SYSTEMOPENMPI,FOAM_INST_DIR='"$tmpdir"',g' $tmpdir/$pkg/etc/bashrc 

## change WM_PROJECT_DIR to temp directory
# sed -i 's,export WM_PROJECT_DIR=$WM_PROJECT_INST_DIR\/$WM_PROJECT-$WM_PROJECT_VERSION,export WM_PROJECT_DIR='"${tmpdir}"'/'"${pkg}"',' $tmpdir/$pkg/etc/bashrc

## Change label size to 64 for 9.22*e18 max. cells
# sed -i 's/export WM_LABEL_SIZE=32/export WM_LABEL_SIZE=64/' $tmpdir/$pkg/etc/bashrc

## Use system metis
# sed -i 's,export METIS_ARCH_PATH=$WM_THIRD_PARTY_DIR\/platforms\/$WM_ARCH$WM_COMPILER$WM_PRECISION_OPTION$WM_LABEL_OPTION\/$METIS_VERSION,export METIS_ARCH_PATH=${SCINET_METIS_ROOT},' $tmpdir/$pkg/etc/bashrc

## Specify ThirdParty location
# sed -i 's,export WM_THIRD_PARTY_DIR=$WM_PROJECT_INST_DIR\/ThirdParty-$WM_PROJECT_VERSION,export WM_THIRD_PARTY_DIR='"${tmpdir}"'\/ThirdParty-$WM_PROJECT_VERSION,' $tmpdir/$pkg/etc/bashrc

## Disable OF/gperftools
# mv $tmpdir/$pkg/etc/config.sh/gperftools $tmpdir/$pkg/etc/config.sh/gperftools.org

## Change compilers
sed -i "s/export WM_CC='gcc'/export WM_CC='mpicc'/" etc/config.sh/settings
sed -i "s/export WM_CXX='g++'/export WM_CXX='mpicxx'/" etc/config.sh/settings

## Compiler settings
sed -i 's/cc          = gcc -m64/cc          = mpicc -m64/' wmake/rules/linux64Gcc/c
sed -i 's/CC          = g++ -std=c++11 -m64/CC          = mpicxx -std=c++11 -m64/' wmake/rules/linux64Gcc/c++

## Get rid of unalias otherwise cooperage fails
sed -i 's/alias wmUnset/#alias wmUnset/' etc/config.sh/aliases
sed -i 's/unalias wmRefresh/#unalias wmRefresh/' etc/config.sh/aliases
sed -i '77s/else/#else/' etc/config.sh/aliases

################################################################
## BEFORE INSTALLING OPENFOAM DO SOME THIRDPARTY INSTALLATION ##
################################################################

## Now go back to same level as OpenFOAM-6.0 directory
cd ..

## download thirdparty
wget -O - http://dl.openfoam.org/third-party/6 | tar xz
mv ThirdParty-6-version-6 ThirdParty-6
cd ThirdParty-6
mkdir download

# Download extra packages needed by openfoam-6
#wget -P download https://www.cmake.org/files/v3.9/cmake-3.9.0.tar.gz
#wget -P download https://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2
wget -P download https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.10/CGAL-4.10.tar.xz
wget -P download https://www.open-mpi.org/software/ompi/v2.1/downloads/openmpi-2.1.1.tar.bz2

# Extract
#tar -xzf download/cmake-3.9.0.tar.gz
#tar -xjf download/boost_1_55_0.tar.bz2
tar -xJf download/CGAL-4.10.tar.xz
tar -xjf download/openmpi-2.1.1.tar.bz2

# Go back to the main level
cd ..

# Update versions
#sed -i -e 's/\(boost_version=\)boost-system/\1boost_1_55_0/' OpenFOAM-6/etc/config.sh/CGAL
sed -i -e 's/\(cgal_version=\)cgal-system/\1CGAL-4.10/' OpenFOAM-6/etc/config.sh/CGAL

## Ignore warnings - Source OF environment to initiate installation
set +u
  . $tmpdir/$pkg/etc/bashrc \
    WM_LABEL_SIZE=32 \
    WM_COMPILER_TYPE=system \
    WM_COMPILER=Gcc \
    WM_MPLIB=SYSTEMOPENMPI \
    FOAMY_HEX_MESH=yes
# set -u

##############################
## RUN THIRDPARTY INSTALLER ##
##############################
cd ThirdParty-6


#sed -i -e 's,_foamAddLib $CGAL_ARCH_PATH\/lib,_foamAddLib $CGAL_ARCH_PATH\/lib64,' $tmpdir/$pkg/etc/config.sh/CGAL
#sed -i -e 's,-L$(CGAL_ARCH_PATH)\/lib,-L$(CGAL_ARCH_PATH)\/lib64,' $tmpdir/$pkg/wmake/rules/General/CGAL
#sed -i -e 's,thirdPartyPath=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER,thirdPartyPath=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER$WM_ARCH_OPTIONInt$WM_LABEL_SIZE,' $tmpdir/$pkg/etc/config.sh/CGAL
#sed -i -e 's,installBASE=$WM_THIRD_PARTY_DIR\/platforms\/$WM_ARCH$WM_COMPILER_ARCH,installBASE=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER$WM_ARCH_OPTIONInt$WM_LABEL_SIZE,' makeCGAL

## Using system BOOST, GMP and MPFR
sed -i -e 's,BOOST_ARCH_PATH=$installBASE\/$boostPACKAGE,BOOST_ARCH_PATH=${SCINET_BOOST_ROOT},' makeCGAL
sed -i -e 's,GMP_ARCH_PATH=$installBASE\/$gmpPACKAGE,GMP_ARCH_PATH=${SCINET_GMP_ROOT},' makeCGAL
sed -i -e 's,MPFR_ARCH_PATH=$installBASE\/$mpfrPACKAGE,MPFR_ARCH_PATH=${SCINET_MPFR_ROOT},' makeCGAL

## libmpfr.so is in ${SCINET_MPFR_ROOT}/lib not /lib64
sed -i -e 's,-DMPFR_LIBRARIES=$MPFR_ARCH_PATH\/lib$WM_COMPILER_LIB_ARCH\/libmpfr.so,-DMPFR_LIBRARIES=$MPFR_ARCH_PATH/lib/libmpfr.so,' makeCGAL

## libgmp.so and libgmpxx.so are also in ${SCINET_GMP_ROOT}/lib not /lib64
sed -i -e 's,-DGMP_LIBRARIES=$GMP_ARCH_PATH\/lib$WM_COMPILER_LIB_ARCH\/libgmp.so,-DGMP_LIBRARIES=$GMP_ARCH_PATH\/lib\/libgmp.so,' makeCGAL
sed -i -e 's,-DGMPXX_LIBRARIES=$GMP_ARCH_PATH\/lib$WM_COMPILER_LIB_ARCH\/libgmpxx.so,-DGMPXX_LIBRARIES=$GMP_ARCH_PATH\/lib\/libgmpxx.so,' makeCGAL

## Compile CMake
# ./makeCmake 2>&1 | tee log.makeCmake
# . $tmpdir/$pkg/etc/bashrc 

## Compile CGAL
./makeCGAL 2>&1 | tee log.makeCGAL
. $tmpdir/$pkg/etc/bashrc 

## Compile ThirdParty
./Allwmake 2>&1 | tee log.makeThirdParty
. $tmpdir/$pkg/etc/bashrc 

############################
## RUN OPENFOAM INSTALLER ##
############################

cd $tmpdir/$pkg

## Ignore warnings - Source OF environment to initiate installation
#set +u
#  . etc/bashrc 
# set -u

./Allwmake -j 20 2>&1 | tee log.Allwmake
## The "-k" option should build everything and ignore the parts that fail to build
## Intended for foamyHexMesh and foamyQuadMesh
# ./Allwmake -j 20 -k 2>&1 | tee log.Allwmake

## Create soft links for libPstream.so and libptscotchDecomp.so
#ln -s platforms/linux64GccDPInt64Opt/lib/openmpi-system/libPstream.so platforms/linux64GccDPInt64Opt/lib/libPstream.so 
#ln -s platforms/linux64GccDPInt64Opt/lib/openmpi-system/libptscotchDecomp.so platforms/linux64GccDPInt64Opt/lib/libptscotchDecomp.so

#################################################
## COPY INSTALLED OPENFOAM TO PREFIX DIRECTORY ##
#################################################

## Change FOAM_INST_DIR to SCINET_OF_ROOT to make it work on the cluster when it is loaded as a module
sed -i 's,FOAM_INST_DIR='"$tmpdir"',FOAM_INST_DIR=${SCINET_OPENFOAM_ROOT},' etc/bashrc
sed -i 's,export WM_PROJECT_DIR='"${tmpdir}"'\/'"${pkg}"',export WM_PROJECT_DIR=${SCINET_OPENFOAM_ROOT}\/'"$pkg"',' etc/bashrc
sed -i 's,export WM_THIRD_PARTY_DIR='"${tmpdir}"'\/ThirdParty-$WM_PROJECT_VERSION,export WM_THIRD_PARTY_DIR=${SCINET_OPENFOAM_ROOT}\/ThirdParty-$WM_PROJECT_VERSION,' etc/bashrc

## Now copy entire OF directory to the 
## prefix=/scinet/niagara/opt/... with proper permissions
cd ..
chmod u+w $pkg ThirdParty-6 -R
cp -ar $pkg ThirdParty-6 $prefix/
