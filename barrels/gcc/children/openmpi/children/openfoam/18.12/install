#!/bin/bash

#############################################################
## This installer is designed to compile OpenFOAM on       ##
## Niagara at Scinet. Installation can be started by       ##
## ./cooper-install gcc/7.3.0@openmpi/3.1.1@openfoam/18.12 ##
## F.Ertinaz | 2019.02.25                                  ##
#############################################################

prefix="$1"
module load $2
module load $3

module load cmake
module load boost/1.66.0 fftw/3.3.7 metis/5.1.0

### Download openfoam
pkg=OpenFOAM
vrs=v1812
wget https://sourceforge.net/projects/openfoamplus/files/$vrs/$pkg-$vrs.tgz
tar -xaf $pkg-$vrs.tgz
# rm -f $pkg-$vrs.tgz

### Download thirdparty
wget https://sourceforge.net/projects/openfoamplus/files/$vrs/ThirdParty-$vrs.tgz
tar -xaf ThirdParty-$vrs.tgz
# rm -f ThirdParty-$vrs.tgz

### Work in temp folder
export ROOT=$PWD

### Change dir to OpenFOAM-v1812
cd $pkg-$vrs

### Install OF in the temporary /dev/shm 
sed -i '54s/projectDir/#projectDir/' etc/bashrc
sed -i '55s/\[/#\[/' etc/bashrc
sed -i 's,$HOME\/OpenFOAM\/OpenFOAM-$WM_PROJECT_VERSION,'"$ROOT"'\/'"$pkg-$vrs"',' etc/bashrc 
sed -i 's,$projectDir,'"$ROOT"'\/'"$pkg-$vrs"',' etc/bashrc 

### Ignore third-parties --- WHY?? Check earlier versions and correct if needed
# sed -i 's/export WM_THIRD_PARTY_DIR/#export WM_THIRD_PARTY_DIR/' $ROOT/$pkg/etc/bashrc

### Use system FFTW
sed -i 's/fftw_version=fftw-3.3.7/fftw_version=fftw-system/' etc/config.sh/FFTW
sed -i 's,FFTW_ARCH_PATH=$WM_THIRD_PARTY_DIR\/platforms\/$WM_ARCH$WM_COMPILER\/$fftw_version,FFTW_ARCH_PATH=${SCINET_FFTW_ROOT},' etc/config.sh/FFTW

### Disable gperftools
sed -i 's/gperftools_version=gperftools-2.5/gperftools_version=gperftools-none/' etc/config.sh/gperftools

### Use system boost
sed -i 's/boost_version=boost_1_64_0/boost_version=boost-system/' etc/config.sh/CGAL
sed -i 's,BOOST_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$boost_version,BOOST_ARCH_PATH=${SCINET_BOOST_ROOT},' etc/config.sh/CGAL

### Use system metis
sed -i 's/METIS_VERSION=metis-5.1.0/METIS_VERSION=metis-system/' etc/config.sh/metis
sed -i 's,METIS_ARCH_PATH=$WM_THIRD_PARTY_DIR\/platforms\/$WM_ARCH$WM_COMPILER$WM_PRECISION_OPTION$WM_LABEL_OPTION\/$METIS_VERSION,METIS_ARCH_PATH=${SCINET_METIS_ROOT},' etc/config.sh/metis

### Ignore warnings - Source OF environment to initiate installation
set +eu
  . etc/bashrc
set -eu

### Run OF installer
./Allwmake -j $(nproc) 2>&1 | tee log.Allwmake

### Compile decomposition methods separately
cd src/parallel/decompose
./Allwmake 2>&1 | tee log.Allwmake.decompose
# set -u

### Now copy entire OF directory to the prefix=/scinet/niagara/opt/... with proper permissions
cd $ROOT
chmod u+rw $pkg-$vrs ThirdParty-$vrs -R
cp -r $pkg-$vrs ThirdParty-$vrs $prefix/

### Change FOAM_INST_DIR to SCINET_OF_ROOT to make it work on the cluster when it is loaded as a module
sed -i 's,'"$ROOT"',${SCINET_OPENFOAM_ROOT},' $prefix/$pkg-$vrs/etc/bashrc 
# sed -i 's/export WM_PROJECT_DIR=$WM_PROJECT_INST_DIR\/$WM_PROJECT-$WM_PROJECT_VERSION/export WM_PROJECT_DIR=${SCINET_OPENFOAM_ROOT}/g' $prefix/etc/bashrc 
