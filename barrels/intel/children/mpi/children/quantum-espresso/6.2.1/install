#!/bin/bash

prefix="$1"
module load "${@:2}"

which mpiicpc   >/dev/null && mpi=intelmpi
which ompi_info >/dev/null && mpi=openmpi

# based on: https://github.com/hfp/xconfigure/
#   config/qe/configure-qe-skx-omp.sh
#   config/elpa/configure-elpa-skx-omp.sh

flags="-O2 -xHost -qopt-zmm-usage=high -fp-model fast=2 -complex-limited-range"
cflags="$flags -fno-alias -ansi-alias"
fflags="$flags -align array64byte -heap-arrays 4096 -threads"
liomp5="-Wl,--as-needed -liomp5 -Wl,--no-as-needed"

export AR=xiar

## ELPA

pkg="elpa-2017.05.003"
archive="$pkg.tar.gz"
url="http://elpa.mpcdf.mpg.de/html/Releases/2017.05.003/$archive"
sha1sum="bea56ee27925dea4fd869e422b994f9a9a038fb9"
wget $url
sha1sum --check <<<"$sha1sum $archive"
tar -xaf $archive
cd $pkg

CFLAGS="$cflags" \
CXXFLAGS="$cflags" \
FCFLAGS="$fflags -I$MKLROOT/include/intel64/lp64" \
LIBS="-lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread $liomp5" \
SCALAPACK_LDFLAGS="-lmkl_scalapack_lp64 -lmkl_blacs_${mpi}_lp64" \
./configure --prefix="$prefix" \
    --enable-avx512 \
    --enable-openmp \
    --host=x86_64-unknown-linux-gnu

make -j$(nproc)
#make check
make install
cd ..

elpa_modules_dir="$prefix/include/$(echo $pkg | sed 's/elpa/elpa_openmp/')/modules"
[[ -d $elpa_modules_dir ]]

## QE

name="qe"
version="6.2.1"
md5sum="e9d92b51a8b9983766a89a164bfac36f"
archive="$name-$version.tar.gz"
url="http://qe-forge.org/gf/download/frsrelease/247/1132/$archive"

wget $url
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd $name-$version

blas_libs="-Wl,--start-group \
    $MKLROOT/lib/intel64/libmkl_intel_lp64.a \
    $MKLROOT/lib/intel64/libmkl_core.a \
    $MKLROOT/lib/intel64/libmkl_intel_thread.a \
    $MKLROOT/lib/intel64/libmkl_blacs_${mpi}_lp64.a \
    -Wl,--end-group"

BLAS_LIBS="$blas_libs" \
CFLAGS="$cflags -O3" \
FFLAGS="$fflags -assume byterecl -qopenmp" \
FFT_LIBS="$blas_libs" \
LAPACK_LIBS="$blas_libs" \
LD_LIBS="$liomp5" \
LDFLAGS="-static-intel -static-libgcc -static-libstdc++ -qopenmp" \
SCALAPACK_LIBS="$MKLROOT/lib/intel64/libmkl_scalapack_lp64.a" \
./configure --prefix="$prefix" \
    --enable-openmp \
    --with-elpa-include="$elpa_modules_dir" \
    --with-elpa-lib="$prefix/lib/libelpa_openmp.a" \
    --with-elpa-version=2017 \
    --with-scalapack=intel

sed -i \
    -e 's/-nomodule -qopenmp/-nomodule/' \
    -e 's/-qopenmp/-qopenmp -qoverride_limits/' \
    -e 's:^IFLAGS\s\s*=\s:IFLAGS = -I$(MKLROOT)/include/fftw -I$(MKLROOT)/include :' \
    -e 's/-D__FFTW3/-D__DFTI -D__EXX_ACE -D__NON_BLOCKING_SCATTER/' \
    make.inc

# parallel make is buggy, so fallback to serial
make all -j$(nproc) || make all

# a few test failures are ok
set +e
make test-suite | tee test.output
set -e
passed=$(egrep -o 'All done. ERROR: only [0-9]+ out of 17 tests passed.' test.output | gawk '{print $5}')
(( $passed > 11 )) || exit 1

make install
