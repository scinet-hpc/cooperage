#!/bin/bash

prefix="$1"
module load $2
module load $3
module load cmake

version="3.8.4"
archive="petsc-$version.tar.gz"
url="http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/$archive"
md5sum="d7767fe2919536aa393eb22841899306"

wget $url
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd petsc-$version

mkllib="$MKLROOT/lib/intel64"
which mpiicpc   >/dev/null && mpi=intelmpi
which ompi_info >/dev/null && mpi=openmpi
optflags="-march=native -O3"

./configure --prefix="$prefix" \
    CC="$CC" \
    CXX="$CXX" \
    F77="$F77" \
    F90="$F90" \
    FC="$FC" \
    COPTFLAGS="$optflags" \
    CXXOPTFLAGS="$optflags" \
    FOPTFLAGS="$optflags" \
    --download-chaco=1 \
    --download-hypre=1 \
    --download-metis=1 \
    --download-ml=1 \
    --download-mumps=1 \
    --download-parmetis=1 \
    --download-plapack=1 \
    --download-prometheus=1 \
    --download-ptscotch=1 \
    --download-scotch=1 \
    --download-sprng=1 \
    --download-superlu=1 \
    --download-superlu_dist=1 \
    --download-triangle=1 \
    --with-blas-lapack-dir="$MKLROOT" \
    --with-debugging=0 \
    --with-mkl_pardiso-dir="$MKLROOT" \
    --with-scalapack=1 \
    --with-scalapack-lib="[${mkllib}/libmkl_scalapack_lp64.so,${mkllib}/libmkl_blacs_${mpi}_lp64.so]" \
    --with-x=0

make all
make test
make install
