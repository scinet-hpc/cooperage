#!/bin/bash

prefix="$1"
module load sqlite

version="3.6.5"
archive="Python-$version.tar.xz"
url="https://www.python.org/ftp/python/$version/$archive"
md5sum="9f49654a4d6f733ff3284ab9d227e9fd"

wget $url
md5sum --check <<<"$md5sum $archive"
tar -xaf $archive
cd Python-$version

#
# Patch to fix the following test failure:
#
#  ======================================================================
#  FAIL: test_register_chain (test.test_faulthandler.FaultHandlerTests)
#  ----------------------------------------------------------------------
#  Traceback (most recent call last):
#    File "/dev/shm/nolta/cleanenv.QZjdMj/Python-3.6.4/Lib/test/test_faulthandler.py", line 716, in test_register_chain
#      self.check_register(chain=True)
#    File "/dev/shm/nolta/cleanenv.QZjdMj/Python-3.6.4/Lib/test/test_faulthandler.py", line 694, in check_register
#      self.assertEqual(exitcode, 0)
#  AssertionError: -11 != 0
#
# See https://bugs.python.org/issue22503
#
sed -i 's/SIGSTKSZ/2*SIGSTKSZ/' Modules/faulthandler.c

# Patch to fix text_asyncio & test_multiprocessing_forkserver failures,
# due to increased tcp socket buffer sizes (/etc/sysctl.d/*.conf).
sed -i 's/(1024\*1024)/(16*1024*1024)/' Lib/test/_test_multiprocessing.py
sed -i 's/PIPE_MAX_SIZE = 4/PIPE_MAX_SIZE = 16/' Lib/test/support/__init__.py

CC="core-gcc" \
CXX="core-g++" \
CFLAGS="-march=native $(pkg-config --cflags-only-other sqlite3)" \
CPPFLAGS="$(pkg-config --cflags-only-I sqlite3)" \
LDFLAGS="$(pkg-config --libs sqlite3)" \
LD_RUN_PATH="$(pkg-config --variable=libdir sqlite3)" \
./configure --prefix="$prefix" \
    --enable-ipv6 \
    --enable-optimizations \
    --enable-shared \
    --with-system-ffi \
    --with-threads

make -j$(nproc)
make test
make install
ln -s python3 "$prefix/bin/python"
cd ..

PATH="$prefix/bin:$PATH"
LD_LIBRARY_PATH="$prefix/lib:$LD_LIBRARY_PATH"
export PYTHONHOME="$prefix"

pip3 install virtualenv
