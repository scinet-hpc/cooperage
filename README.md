# Cooperage

An experimental HPC cluster software build system.
Similar to easybuild and spack, but simpler and dumber.

Software packages are referred to as "barrels", hence the name cooperage.

## Installing barrels

Barrels are installed via the `cooper-install` script,
which takes a single argument, the barrel name.
For example,

```sh
./cooper-install foobar/1.2.3
```

This does the following:

* Finds the barrel directory, in this case `./barrels/foobar/1.2.3/`.
* Runs the `install` script in the barrel directory.
* Creates a module file by inspecting the install directory.
* (optional) Appends `modulefooter` to the module.
* (optional) Runs `test`.

See [barrels/python/3.6.5](barrels/python/3.6.5) for example.

## Creating barrels

### Example 1: "Hello World"

Create the directory `./barrels/helloworld/1.0/`
containing a file named `install` with the following body:

```bash
#!/bin/bash
prefix="$1"

mkdir "$prefix/bin"

exe="$prefix/bin/helloworld"
cat > "$exe" <<___
#!/bin/sh
echo hello world
___
chmod +x "$exe"
```

Then run the command:

```sh
./cooper-install helloworld/1.0
```

This runs the `install` script with a single argument, the install prefix:

```sh
install $prefix
```

The module file is automatically created by inspecting `$prefix`.
In this case, `$prefix/bin` will be prepended to `$PATH`.

```sh
$ module load helloworld/1.0
$ helloworld
hello world
```

### Example 2: "Hello World" in C

Barrels can be nested in order to create
[hierarchical modules](http://lmod.readthedocs.io/en/latest/080_hierarchy.html).
These "sub-barrels" are found in a `children` subdirectory of the parent barrel.

Create the directory:

```
./barrels/gcc/7.3.0/children/helloworld/1.0/
```

```bash
prefix="$1"
module load $2  # compiler

mkdir "$prefix/bin"

cat > helloworld.c <<___
#include <stdio.h>
int main(void) { printf("hello world\n"); return 0; }
___

$CC helloworld.c -o "$prefix/bin/helloworld"
```

Now when running `cooper-install` you have to specify both the parent and
child, separated by an `@`:

```sh
./cooper-install gcc/7.3.0@helloworld/1.0
```

The `install` script is passed an additional argument, the parent module name:

```sh
install $prefix gcc/7.3.0
```

### Example 3: MPI "Hello World"

Barrels can be nested arbitrarily deep.

Create the directory:

```
./barrels/gcc/7.3.0/children/openmpi/2.1.2/children/helloworld/1.0/
```

```bash
prefix="$1"
module load $2  # compiler
module load $3  # mpi

mkdir "$prefix/bin"

cat > helloworld.c <<___
#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    printf("hello world from %d\n", rank);
    MPI_Finalize();
    return 0;
}
___

mpicc helloworld.c -o "$prefix/bin/helloworld"
```

```sh
./cooper-install gcc/7.3.0@openmpi/2.1.2@helloworld/1.0
```

The `install` script is passed the prefix and both parent module names:

```sh
install $prefix gcc/7.3.0 openmpi/2.1.2
```

### Generic versions

Hierarchical modules are usually only weakly dependent on their parent module version.
For example, if we have a barrel `foo/A@bar/X`, and `foo/B` is released,
then the `barrels/foo/B/children/bar/X` directory will usually just contain
an identical copy of the files in `barrels/foo/A/children/bar/X`.

To avoid this, `cooper-install` looks for sub-barrels in multiple
subdirectories, and uses the first one it finds.
For example, `./cooper-install foo/B@bar/X` will look in:

* `foo/B/children`, for sub-barrels that require version B of foo;
* `foo/children`, for sub-barrels that don't depend on the version of foo;
* `foo/family/children`, for sub-barrels that don't depend on foo, but will
accept any package in the same family as foo.
