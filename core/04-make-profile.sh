#!/bin/bash
set -eu

. ../config

cat >$software_root/profile.sh <<___
if [ -z \${MODULESHOME+x} ]; then
    # core
    PATH="$core_prefix/bin:\$PATH"
    LD_LIBRARY_PATH="$core_prefix/lib64\${LD_LIBRARY_PATH+:}\${LD_LIBRARY_PATH-}"
    export LD_LIBRARY_PATH

    # lmod clobbers BASH_ENV, so save and restore
    [ ! -z \${BASH_ENV+x} ] && old_bash_env="\$BASH_ENV"

    # modules
    . $software_root/lmod/lmod/init/profile

    if [ ! -z \${old_bash_env+x} ]; then
        BASH_ENV="\$old_bash_env"
        unset old_bash_env
    fi
fi
___

echo ok
